// Manipulating replay output
// Similar to standard media device controls
// Fast Rewind, play, pause, Fast Forward
package main

import (
    "fmt"
)

// Rewind back output
func (t *gtrTextGrid) fastRewind() {
    fmt.Println("fastRewind!")
}

// play output
func (t *gtrTextGrid) play() {
    fmt.Println("play!")
}

// pause playing output
func (t *gtrTextGrid) pause() {
    fmt.Println("pause!")
}

// Fast forward output
func (t *gtrTextGrid) fastForward() {
    fmt.Println("fastForward!")
}

func (t *gtrTextGrid) loadScript() {
    fmt.Println("Load Script File!")
}
