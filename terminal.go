// Replay output configuration
// widget, basic structures
package main

import (
    "image/color"

    "fyne.io/fyne"
    "fyne.io/fyne/canvas"
    "fyne.io/fyne/widget"
)

type gtrTextGrid struct {
    widget.TextGrid

    Focused bool
    Columns, Rows int
}

func newGtrTextGrid() *gtrTextGrid {
    t := &gtrTextGrid{}
    t.ExtendBaseWidget(t)
    t.Focused = true
    t.Columns = 80
    t.Rows = 24

    return t
}

// Resizing helper function
func guessCellSize() fyne.Size {
    cell := canvas.NewText("M", color.White)
    cell.TextStyle.Monospace = true

    return cell.MinSize()
}

// Term size depends on the current font parameters and Columns * Rows of the current content
func (t *gtrTextGrid) TermSize() fyne.Size {
    cell := canvas.NewText("M", color.White)
    cell.TextStyle.Monospace = true

    return fyne.NewSize(cell.MinSize().Width * t.Columns, cell.MinSize().Height * t.Rows)
}

// Resize is called when this terminal widget has been resized.
// It ensures that the virtual terminal is within the bounds of the widget.
// func (t *gtrTextGrid) Resize(s fyne.Size) {
// 	if s.Width == t.Size().Width && s.Height == t.Size().Height {
// 		return
// 	}
// 	if s.Width < 20 { // not sure why we get tiny sizes
// 		return
// 	}
// 	t.BaseWidget.Resize(s)
// 	t.Content.Resize(s)
// }

