// Layout of the application
package main

import (
    "fyne.io/fyne/theme"
    "fyne.io/fyne/widget"
)

// Idea is to automatically have all of the controls in the center
// but separated logically by functionality
func gtrToolbar(t *gtrTextGrid) *widget.Toolbar {
    return widget.NewToolbar(
        widget.NewToolbarSpacer(),
        widget.NewToolbarAction(theme.FileIcon(), func() {
            t.loadScript()
        }),
        widget.NewToolbarSeparator(),
        widget.NewToolbarAction(theme.MediaFastRewindIcon(), func() {
            t.fastRewind()
        }),
        widget.NewToolbarAction(theme.MediaPlayIcon(), func() {
            t.play()
        }),
        widget.NewToolbarAction(theme.MediaPauseIcon(), func() {
            t.pause()
        }),
        widget.NewToolbarAction(theme.MediaFastForwardIcon(), func() {
            t.fastForward()
        }),
    )
}
