// Let's run the app
package main

import (
	"fyne.io/fyne"
	"fyne.io/fyne/app"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
)

func main() {
	a := app.New()
	w := a.NewWindow("GTermReplay")
	w.SetPadded(false)

	t := newGtrTextGrid()
	l := newTimeLabel()
	manipulators := gtrToolbar(t)
	toolbar := widget.NewHBox(layout.NewSpacer(),
		manipulators, l,
		layout.NewSpacer())

	content := fyne.NewContainerWithLayout(
		layout.NewBorderLayout(nil, toolbar, nil, nil),
		toolbar, widget.NewScrollContainer(t))

	w.Resize(t.TermSize())

	w.SetContent(content)
	w.ShowAndRun()
}
