// Display and manage time
package main

import (
    "time"

    "fyne.io/fyne"
    "fyne.io/fyne/widget"
)

type timeLabel struct {
    widget.Label
}

func newTimeLabel() *timeLabel {
    l := &timeLabel{}
    l.TextStyle.Bold = true
    l.Alignment = fyne.TextAlignCenter
    l.Wrapping = fyne.TextWrapOff
    l.Text = time.Now().Format(time.RFC1123Z)
    return l
}

func (l *timeLabel) update() {
    l.Text = time.Now().Format(time.RFC1123Z)
}
